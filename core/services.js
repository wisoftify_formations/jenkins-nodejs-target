var fs = require("fs");
const {loggerTabbed} = require("./helpers");
const emoji = require("node-emoji");
const { DEEPLEVEL_INCREMENT } = require("./consts");

const registerFolder = (path, indent = 0) => {
  let files = fs.readdirSync(path);
  let services = {};

  loggerTabbed(`${emoji.get("open_file_folder")} ${path.replace(process.cwd(), "").replace("\\", "/")}`, indent);

  files.forEach((file) => {
    if (file.startsWith("_")) return;//skip files with underscore to avoid load of helpers

    if (fs.lstatSync(path + "/" + file).isDirectory()) {
      services[file] = registerFolder(path + "/" + file, indent + DEEPLEVEL_INCREMENT);
    } else if (file === "index.js") {
      const _package = require(path + "/" + file);
      const folderName = path.split("/").pop();
      services[folderName] = _package;
      loggerTabbed(`${emoji.get("package")} ${path.replace(process.cwd(), "").replace("\\", "/")}/${file}: ${Object.keys(_package).join(", ")}`, indent + DEEPLEVEL_INCREMENT);
    } else {
      const _package = require(path + "/" + file);
      services[file.replace(".js", "")] = _package;
      loggerTabbed(`${emoji.get("package")} ${path.replace(process.cwd(), "").replace("\\", "/")}/${file}: ${Object.keys(_package).join(", ")}`, indent + DEEPLEVEL_INCREMENT);
    }
  });

  return services;
}

const services =  registerFolder(`${process.cwd()}/services`);
module.exports = services;