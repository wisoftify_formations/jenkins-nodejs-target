const {PrismaClient} = require("@prisma/client");
const emoji = require("node-emoji");

module.exports = () => {
  if (!process.env.DATABASE_URL)
    throw new Error("DATABASE_URL is not set, check config/env.js");
  const prisma = new PrismaClient();
  console.log("Prisma initialized:", process.env.DATABASE_URL, emoji.get("white_check_mark"));
  return prisma;
}