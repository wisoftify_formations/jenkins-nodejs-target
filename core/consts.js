module.exports = {
  DEEPLEVEL_INCREMENT: 4,
  METHOD_EMOJI: {"get": "green_book", "post": "orange_book", "put": "blue_book", "delete": "closed_book"},
}