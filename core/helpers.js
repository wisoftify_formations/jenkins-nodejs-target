var pluralize = require('pluralize');
var colors = require('colors');
var emoji = require("node-emoji")

module.exports = {
  loggerTabbed: (log, indent = 0) => console.log(" ".repeat(indent) + log),
  routeFromPath: (path) => (
    path.replace(/.*(\/controllers)/g, "")
        .replaceAll("\\", "/")
        .replace(".js", "")
        .split("/")
        .map((folder, index, arr) => {
          if (index === 0) return folder;
          if (index === arr.length - 1) return folder;
          return `${folder}/:${pluralize.singular(folder)}Id`;
        })
        .join("/")
  ),
  launcherScreen: () => {
    console.log("");
    console.log(colors.green("  88888888      d8888 888888b.  8888888 "));
    console.log(colors.green("    \"88b       d88888 888  \"88b   888   "));
    console.log(colors.green("     888      d88P888 888  .88P   888   "));
    console.log(colors.green("     888     d88P 888 8888888K.   888   "));
    console.log(colors.green("     888    d88P  888 888  \"Y88b  888   "));
    console.log(colors.green("     888   d88P   888 888    888  888   "));
    console.log(colors.green("     88P  d8888888888 888   d88P  888   "));
    console.log(colors.green("     888 d88P     888 8888888P\" 8888888 "));
    console.log(colors.green("   .d88P                                "));
    console.log(colors.green(" .d88P\"                                 "));
    console.log(colors.green("888P\"   "));
    console.log("");
  },
  showEmojiDataset: (search = "") => emoji.search(search).map(({name, emoji}) => console.log(`${emoji} ${name}`))
};