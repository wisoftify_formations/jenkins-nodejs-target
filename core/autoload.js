const express = require("express");
const {loggerTabbed, routeFromPath} = require("./helpers");
const { DEEPLEVEL_INCREMENT } = require("./consts");
const emoji = require("node-emoji");
const {METHOD_EMOJI} = require("./consts");

module.exports = {
  default: (route, filename, deepLevel) => {
    var router = express.Router();
    const path = route.path ?? routeFromPath(filename);
    loggerTabbed(`${emoji.get("books")} Route: ${path}`, deepLevel);

    if (route.middlewares) {
      Object.entries(route.middlewares).forEach(([key, value]) => {
        loggerTabbed(`${emoji.get("arrow_right")} Middleware: ${key}`, deepLevel + DEEPLEVEL_INCREMENT);
        router.use(value);
      });
    }
    Object.entries(route?.routes ?? {}).forEach(([key, value]) => {
      if (["path"].includes(key)) return;
      loggerTabbed(`${emoji.get(METHOD_EMOJI[value.method])} ${value.method.toUpperCase()} ${key} (${path}${value.path})`, deepLevel + DEEPLEVEL_INCREMENT);
      router[value.method](
        `${path}${value.path}`,
        value.middlewares ?? [],
        async (req, res) => { // try/catch wrapper
          try {
            await value.handler(req, res);
          } catch (e) {
            console.error(e);
            res.status(500).send("Internal server error");
          }
        }
      );
    });

    return router;
  }
}