
let notes = {

};

module.exports = {
  routes: {
    get: {
      method: "get",
      path: "/",
      handler: async (req, res) => {
        const id = req.headers.userid;

        if (!notes[id])
          return res.json([]);
        return res.json(notes[id]);
      },
    },
    post: {
      method: "post",
      path: "/",
      handler: async (req, res) => {
        const id = req.headers.userid;

        notes = {
          ...notes,
          [id]: [
            ...(notes[id] ?? []),
            {
              uid: Math.floor(Math.random() * 10000000000),
              title: req.body.title,
              content: req.body.content
            }
          ]
        }

        return res.json(notes[id]);
      }
    },
    delete: {
      path: "/:id",
      method: "delete",
      handler: async (req, res) => {
        const id = req.headers.userid;
        const noteId = req.params.id;

        notes = {
          ...notes,
          [id]: notes[id].filter((note) => note.uid != noteId)
        }

        return res.json(notes[id]);
      }
    },
  }
}