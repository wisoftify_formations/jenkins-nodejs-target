//read files in this folder and export them as an object

const fs = require('fs');
const path = require('path');

const files = fs.readdirSync(__dirname);

files.forEach((file) => {
  if (!["index.js", ".env"].includes(file)) {
    const fileName = path.basename(file, '.js');
    module.exports[fileName] = require(`./${fileName}`);
  }
});